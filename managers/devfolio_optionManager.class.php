<?php


class devfolio_optionManager {

    protected static $instance = null;

    private $devfolio_options = array();

    const DEVFOLIO_OPTIONS = 'devfolio_options';

    const DEVFOLIO_GITHUB_USERNAME = 'devfolio_github_username';
    const DEVFOLIO_GITHUB_CLIENT_ID = 'devfolio_github_client_id';
    const DEVFOLIO_GITHUB_CLIENT_SECRET = 'devfolio_github_client_secret';
    const DEVFOLIO_GITHUB_TOKEN = 'devfolio_github_token';
    const DEVFOLIO_GITHUB_LAST_CACHE_LOAD = 'devfolio_last_cache_load';
    const DEVFOLIO_IS_CACHING = 'devfolio_caching';
    const DEVFOLIO_VERSION = 'devfolio_version';


    private function __construct()
    {
    }

    public static function Instance()
    {

        if (!(isset(self::$instance))) {

            self::$instance = new devfolio_optionManager();
            self::$instance->load_options();
        }

        return self::$instance;

    }

    public function init_options() {

        if (!isset($this->devfolio_options)) {
            $this->devfolio_options[self::DEVFOLIO_GITHUB_USERNAME] = '';
            $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_ID] = '';
            $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_SECRET] = '';
            $this->devfolio_options[self::DEVFOLIO_GITHUB_TOKEN] = '';
            $this->devfolio_options[self::DEVFOLIO_GITHUB_LAST_CACHE_LOAD] = '';
            $this->devfolio_options[self::DEVFOLIO_IS_CACHING] = false;
            $this->devfolio_options[self::DEVFOLIO_VERSION] = '1.0';

            add_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
        }


    }

    public function erase_options() {

        if (isset($this->devfolio_options)) {
            delete_option(self::DEVFOLIO_OPTIONS);
            $this->devfolio_options = null;
        }
    }

    public function load_options() {

        self::$instance->devfolio_options = get_option(self::DEVFOLIO_OPTIONS);

    }

    public function are_options_valid() {

        if ($this->devfolio_options[self::DEVFOLIO_GITHUB_USERNAME] != ''
        &&$this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_ID] != ''
        &&$this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_SECRET] != ''
        &&$this->devfolio_options[self::DEVFOLIO_GITHUB_TOKEN] != '' )
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function get_github_username() {

        return $this->devfolio_options[self::DEVFOLIO_GITHUB_USERNAME];
    }

    public function get_github_clientId() {

        return $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_ID];
    }

    public function get_github_client_secret() {

        return $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_SECRET];
    }

    public function get_github_token() {

        return $this->devfolio_options[self::DEVFOLIO_GITHUB_TOKEN];
    }

    public function get_last_cache_load() {

        return $this->devfolio_options[self::DEVFOLIO_GITHUB_LAST_CACHE_LOAD];
    }

    public function get_is_caching() {

        return $this->devfolio_options[self::DEVFOLIO_IS_CACHING];
    }

    public function get_version() {

        return $this->devfolio_options[self::DEVFOLIO_VERSION];
    }


    public function set_github_username($username) {

        $this->devfolio_options[self::DEVFOLIO_GITHUB_USERNAME] = $username;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_github_clientId($client_id) {

        $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_ID] = $client_id;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_github_client_secret($client_secret) {

        $this->devfolio_options[self::DEVFOLIO_GITHUB_CLIENT_SECRET] = $client_secret;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_github_token($token) {

        $this->devfolio_options[self::DEVFOLIO_GITHUB_TOKEN] = $token;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_last_cache_load($last_cache_load) {

        $this->devfolio_options[self::DEVFOLIO_GITHUB_LAST_CACHE_LOAD] = $last_cache_load;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_is_caching($is_caching) {

        $this->devfolio_options[self::DEVFOLIO_IS_CACHING] = $is_caching;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }

    public function set_version($version) {

        $this->devfolio_options[self::DEVFOLIO_VERSION] = $version;
        update_option(self::DEVFOLIO_OPTIONS,$this->devfolio_options);
    }






}



?>