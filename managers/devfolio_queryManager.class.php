<?php

/**
 * Created by PhpStorm.
 * User: Valentin
 * Date: 14/01/2017
 * Time: 20:24
 */


class devfolio_queryManager {


    private static $instance = null;


    private function __construct() {}


    public static function Instance() {

        if (!isset(self::$instance)) {

            self::$instance = new devfolio_queryManager();
        }

        return self::$instance;

    }

    public function get($url,$args,$decode) {

        $response = wp_remote_get($url,$args);
        $response = wp_remote_retrieve_body($response);
        if ($decode)
        $response = json_decode($response,true);

        return $response;

    }

    public function getBody($url,$args=array()) {

        $response = wp_remote_get($url,$args);
        $response = wp_remote_retrieve_body($response);
        $response = json_decode($response,true);

        return $response;

    }

    public function getHeaders($url) {

        $response = wp_remote_get($url);
        $response = wp_remote_retrieve_headers($response);
        $response = json_decode($response,true);

        return $response;

    }

    public function getStatusCode($url) {

        $response = wp_remote_get($url);
        $code = wp_remote_retrieve_response_code($response);


        return $code;
    }



    public function post($url,$args=array()) {

        $response = wp_remote_post($url,$args);
        $response = wp_remote_retrieve_body($response);
        $response = json_decode($response,true);

        return $response;
    }

    public function put($url) {

        $response = wp_remote_request($url,array(

            'method' => 'PUT'
        ));
        $response = wp_remote_retrieve_body($response);
        $response = json_decode($response,true);

        return $response;

    }

    public function request($url,$method) {

        $response = wp_remote_request($url,array(

            'method' => $method
        ));
        $response = wp_remote_retrieve_body($response);
        $response = json_decode($response,true);

        return $response;

    }



}