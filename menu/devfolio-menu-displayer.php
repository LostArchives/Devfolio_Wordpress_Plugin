<?php
/**
 * Created by PhpStorm.
 * User: Valentin
 * Date: 14/01/2017
 * Time: 20:28
 */


function devfolio_display_settings()
{

?>

    <div class="wrap">

        <h2>DevFolio Plugin Settings ( Last cache Update : <?php echo get_option('devfolio_options')['devfolio_last_cache_load']; ?> )</h2>


        <form method="post" action="options.php">

            <?php settings_fields('devfolio_group_options'); ?>
            <?php do_settings_sections(__FILE__); ?>
            <?php submit_button();?>

        </form>


    </div>


<?php

}



function devfolio_init_register_settings() {


    register_setting('devfolio_group_options','devfolio_options');


//////////////////////////GitHub Section //////////////////////////////////
    add_settings_section('devfolio_github_section',
        'GitHub',
        'devfolio_section_display',
        __FILE__

    );

    add_settings_field('devfolio_github_username',
        'GitHub Username',
        'devfolio_field_display',
        __FILE__,
        'devfolio_github_section',
        array(
            'id' => 'devfolio_github_username_input',
            'name' => "devfolio_options[devfolio_github_username]",
            'option_id' => 'devfolio_github_username'

        )
    );

    add_settings_field('devfolio_github_client_id',
        'GitHub Client Id',
        'devfolio_field_display',
        __FILE__,
        'devfolio_github_section',
        array(
            'id' => 'devfolio_github_client_id_input',
            'name' => "devfolio_options[devfolio_github_client_id]",
            'option_id' => 'devfolio_github_client_id'

        )
    );


    add_settings_field('devfolio_github_client_secret',
        'GitHub Client Secret',
        'devfolio_field_display',
        __FILE__,
        'devfolio_github_section',
        array(
            'id' => 'devfolio_github_client_secret_input',
            'name' => "devfolio_options[devfolio_github_client_secret]",
            'option_id' => 'devfolio_github_client_secret'

        )
    );

    add_settings_field('devfolio_github_token',
        'GitHub Token',
        'devfolio_field_display',
        __FILE__,
        'devfolio_github_section',
        array(
            'id' => 'devfolio_github_token_input',
            'name' => "devfolio_options[devfolio_github_token]",
            'option_id' => 'devfolio_github_token'

        )
    );


}


$devfolio_options = get_option('devfolio_options');

function devfolio_field_display($args) {

    global $devfolio_options;

    $value = '';
    if (isset($devfolio_options[$args['option_id']])) {
        $value = $devfolio_options[$args['option_id']];
    }
    echo "<input id='".$args['id']."' name='".$args['name']."' type='text' value='" .$value. "' />";

}

function devfolio_section_display($args) {


    echo '<p>Enter your '.$args['title'].' information here ! </p>';

}



?>
