<?php

/*
Template Name: Scan
 */

$element_id = $_GET['id'];
$mode = $_GET['mode'];

$dir = "root";

// Run the recursive function 
if ($mode == 'list') {
    $response = getContent($dir);
}


function getContent($dir)
{

    global $element_id;
    $files = array();

    $contents = devfolio_connectionManager::Instance()->select("Select id,name,path,type,size from wp_devfolio_content where path='" . $dir . "' and project_id=" . $element_id);

    foreach ($contents as $content) {

        if ($content['type'] == 'dir') {

            $files[] = array(
                'name' => $content['name'],
                'type' => 'folder',
                'path' => $content['path'] . '/' . $content['name'],
                'items' => getContent($content['path'] . '/' . $content['name'])
            );
        } else {

            $files[] = array(

                'id' => $content['id'],
                'name' => $content['name'],
                'type' => 'file',
                'path' => $content['path'],
                'size' => $content['size'],

            );
        }

    }

    return $files;

}


// This function scans the files folder recursively, and builds a large array

function scan($dir)
{

    $files = array();

    // Is there actually such a folder/file?

    if (file_exists($dir)) {

        foreach (scandir($dir) as $f) {

            if (!$f || $f[0] == '.') {
                continue; // Ignore hidden files
            }

            if (is_dir($dir . '/' . $f)) {

                // The path is a folder

                $files[] = array(
                    "name" => $f,
                    "type" => "folder",
                    "path" => $dir . '/' . $f,
                    "items" => scan($dir . '/' . $f) // Recursively get the contents of the folder
                );
            } else {

                // It is a file

                $files[] = array(
                    "name" => $f,
                    "type" => "file",
                    "path" => $dir . '/' . $f,
                    "size" => filesize($dir . '/' . $f) // Gets the size of this file
                );
            }
        }

    }

    return $files;
}

function getFileContent()
{

    global $element_id;

    $content = devfolio_connectionManager::Instance()->select('Select content from wp_devfolio_content where id=' . $element_id);

    echo $content[0]['content'];
}


// Output the directory listing as JSON

if ($mode == 'list') {
    header('Content-type: application/json');

    echo json_encode(array(
        "name" => "root",
        "type" => "folder",
        "path" => $dir,
        "items" => $response
    ));
} else {
    getFileContent();
}

