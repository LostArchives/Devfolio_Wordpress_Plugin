<?php

/*
Template Name: DevFolio Load Cache
*/

if (devfolio_optionManager::Instance()->are_options_valid()) {

    set_time_limit(0);

    devfolio_optionManager::Instance()->set_is_caching(true);

    devfolio_optionManager::Instance()->load_options();

    devfolio_cacheManager::Instance()->emptyCache();

    devfolio_cacheManager::Instance()->cache_gitHubProjects();

    devfolio_cacheManager::Instance()->cache_profiles();

    devfolio_optionManager::Instance()->set_last_cache_load(date('d-m-Y H:i:s'));

    devfolio_optionManager::Instance()->set_is_caching(false);

}
else
{
    echo 'Missing options, please check devfolio plugin settings :(';
}



?>