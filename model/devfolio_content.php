<?php

class devfolio_content {

    private $id;
    private $name;
    private $path;
    private $sha;
    private $type;
    private $content;

    /**
     * devfolio_content constructor.
     * @param $id
     * @param $name
     * @param $path
     * @param $full_path
     * @param $sha
     * @param $type
     * @param $content
     */
    public function __construct($id, $name, $path, $sha, $type, $content)
    {
        $this->id = $id;
        $this->name = $name;
        $this->path = $path;
        $this->sha = $sha;
        $this->type = $type;
        $this->content = $content;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    /**
     * @return mixed
     */
    public function getSha()
    {
        return $this->sha;
    }

    /**
     * @param mixed $sha
     */
    public function setSha($sha)
    {
        $this->sha = $sha;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }





}





?>