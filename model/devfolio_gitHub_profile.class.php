<?php

require_once('devfolio_baseProfile.class.php');

class devfolio_gitHub_profile extends devfolio_baseProfile {

    private $_bio;

    public function __construct($username,$truename,$avatar_url,$site,$location,$creation_date,$bio) {

        parent::__construct($username,$truename,$avatar_url,$site,$location,$creation_date);
        $this->_bio = $bio;

    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->_bio;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio)
    {
        $this->_bio = $bio;
    }



}


?>