CREATE TABLE IF NOT EXISTS `wp_devfolio_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `nb_bytes` int(11) NOT NULL,
  `percent` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=676 DEFAULT CHARSET=utf8;

ALTER TABLE `wp_devfolio_languages`
  ADD CONSTRAINT `FK_Language_Project` FOREIGN KEY (`project_id`) REFERENCES `wp_devfolio_github_projects` (`id`);
